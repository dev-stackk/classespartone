package com.algebra.classespartone;

public class Car {

    String manufacturer;
    String color;
    String registrationNumber;
    String fuelType;
    double maxSpeed;

    public Car() {
    }

    public Car(String manufacturer, String color, String registrationNumber, String fuelType, double maxSpeed) {
        System.out.println("Constructor invoked");
        this.manufacturer = manufacturer;
        this.color = color;
        this.registrationNumber = registrationNumber;
        this.fuelType = fuelType;
        this.maxSpeed = maxSpeed;
    }

    void drive(double speed) {
        if (speed <= this.maxSpeed) {
            System.out.println("Car is driving with speed of: " + speed + "km/h");
            return;
        }

        System.out.println("Speed is too high");
    }

    void park() {
        System.out.println("Car is parking");
        System.out.println("Car is parked");
    }

    void fuelUpGas(String gasType) {
        if (gasType.equalsIgnoreCase(this.fuelType)) {
            System.out.println("Pouring up gas : " + fuelType);
            return;
        }

        System.out.println("Wrong fuel type");
    }

    @Override
    public String toString() {
        return "Car{" +
                "manufacturer='" + manufacturer + '\'' +
                ", color='" + color + '\'' +
                ", registrationNumber='" + registrationNumber + '\'' +
                ", fuelType='" + fuelType + '\'' +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
