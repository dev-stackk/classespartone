package com.algebra.classespartone;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Phone phone = new Phone("Samsung", "White", 8, true, "Android");

        System.out.println("Phone color: " + phone.color);
        System.out.println("Phone manufacturer: " + phone.manufacturer);
        System.out.println("Phone RAM: " + phone.ram);
        System.out.println("Phone is turned on: " + phone.isTurnedOn);
        System.out.println("Phone OS: " + phone.operationSystem);

        phone.turnOn();
        System.out.println("Phone is turned on: " + phone.isTurnedOn);

        phone.turnOff();
        System.out.println("Phone is turned on: " + phone.isTurnedOn);

        phone.turnOn();
        System.out.println("Phone is turned on: " + phone.isTurnedOn);

        System.out.println("-------------");

        Phone phone2 = new Phone("Huawei", "Black");
        System.out.println(phone2);

        phone.operationSystem = "Android";
        System.out.println(phone2);

        System.out.println("-------------");

        Car car = new Car("Honda","Red", "Fe-4543-po","Disel", 180.5);

        System.out.println(car);
        car.drive(190);
        car.drive(50);

        car.park();

        car.fuelUpGas("Bensin");
        car.fuelUpGas("Disel");

        Car car1 = new Car();
        car1.fuelType = "Benzin";

    }
}
