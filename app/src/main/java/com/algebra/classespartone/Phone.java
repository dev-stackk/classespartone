package com.algebra.classespartone;

import android.util.Log;

public class Phone {

    String manufacturer;
    String color;
    int ram;
    boolean isTurnedOn;
    String operationSystem;

    public Phone() {
    }

    public Phone(String manufacturer, String color, int ram, boolean isTurnedOn, String operationSystem) {
        this.manufacturer = manufacturer;
        this.color = color;
        this.ram = ram;
        this.isTurnedOn = isTurnedOn;
        this.operationSystem = operationSystem;
    }

    public Phone(String manufacturer, String color) {
        this.manufacturer = manufacturer;
        this.color = color;
    }

    void turnOn () {
        if (this.isTurnedOn) {
            Log.i("Phone", "Phone is already turned on");
            return;
        }

        this.isTurnedOn = true;
        Log.i("Phone", "Turning on the phone");
    }


    void turnOff () {
        if (!this.isTurnedOn) {
            Log.i("Phone", "Phone is already turned off");
            return;
        }

        this.isTurnedOn = false;
        Log.i("Phone", "Turning off the phone");
    }

    @Override
    public String toString() {
        return "Phone{" +
                "manufacturer='" + manufacturer + '\'' +
                ", color='" + color + '\'' +
                ", ram=" + ram +
                ", isTurnedOn=" + isTurnedOn +
                ", operationSystem='" + operationSystem + '\'' +
                '}';
    }
}
